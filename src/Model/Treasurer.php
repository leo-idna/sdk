<?php
namespace TrekkSoft\SDK\Model;

use TrekkSoft\SDK\Helper\MoneyHelper;
use Money\Money;
use Money\Currency;

/**
 * Class Treasurer
 * @package TrekkSoft\SDK\Model
 */
class Treasurer
{
    /**
     * @var array
     */
    protected $options;

    /**
     * Discount constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        $options += [
            'id'                    => null,
            'name'                  => null,
            'possibleRefundAmount'  => null,
            'currency'              => null,
        ];

        $this->options = $options;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->options['id'];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->options['name'];
    }

    /**
     * @return Money
     */
    public function getPossibleRefundAmount()
    {
        return MoneyHelper::createFromDecimal($this->options['possibleRefundAmount'],  new Currency($this->options['currency']));
    }
}
