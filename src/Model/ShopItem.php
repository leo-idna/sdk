<?php
namespace TrekkSoft\SDK\Model;

use TrekkSoft\SDK\Helper\MoneyHelper;
use Money\Money;
use Money\Currency;

class ShopItem
{
    /**
     * @var array
     */
    protected $options;

    /**
     * Discount constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        $options += [
            'id'         => null,
            'refId'      => null,
            'title'      => null,
            'quantity'   => null,
            'price'      => null,
            'currency'   => null,
        ];

        $this->options = $options;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->options['id'];
    }

    /**
     * @return int
     */
    public function getRefId()
    {
        return (int)$this->options['refId'];
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->options['title'];
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        return (int)$this->options['quantity'];
    }

    /**
     * @return Money
     */
    public function getPrice()
    {
        return MoneyHelper::createFromDecimal($this->options['price'],  new Currency($this->options['currency']));
    }
}
