<?php
namespace TrekkSoft\SDK\Model;

use Money\Money;
use TrekkSoft\SDK\Helper\MoneyHelper;

/**
 * Class PriceCategory
 * @package TrekkSoft\SDK\Model
 */
class PriceCategory
{
    /**
     * @var array
     */
    protected $options;

    /**
     * PriceCategory constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        $options += [
            'id'             => null,
            'name'           => null,
            'description'    => null,
            'minSeats'       => null,
            'maxSeats'       => null,
            'capacity'       => null,
            'occupancy'      => null,
            'occupancyPaid'  => null,
            'availableSeats' => null,
            'isCustom'       => null,
            'price'          => [
                'amount'     => null,
                'currency'   => null,
            ],
        ];

        $this->options = $options;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->options['id'];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->options['name'];
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->options['description'];
    }

    /**
     * @return int
     */
    public function getMinSeats()
    {
        return (int)$this->options['minSeats'];
    }

    /**
     * @return int
     */
    public function getMaxSeats()
    {
        return (int)$this->options['maxSeats'];
    }

    /**
     * @return Money
     */
    public function getPrice()
    {
        return MoneyHelper::createFromArray($this->options['price']);
    }

    /**
     * @return bool
     */
    public function isCustom()
    {
        return (bool)$this->options['isCustom'];
    }

    /**
     * @return CapacityInfo
     */
    public function getCapacityInfo()
    {
        return new CapacityInfo(
            $this->options['availableSeats'],
            $this->options['capacity'],
            (int)$this->options['occupancy'],
            (int)$this->options['occupancyPaid']
        );
    }
}
