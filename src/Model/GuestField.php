<?php
namespace TrekkSoft\SDK\Model;

/**
 * Class GuestField
 * @package TrekkSoft\SDK\Model
 */
class GuestField
{
    /**
     * @var array
     */
    protected $options;

    /**
     * GuestField constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        //set default values
        $options += [
            'code'          => null,
            'label'         => null,
            'type'          => null,
            'isRequired'    => null,
            'options'       => null,
        ];

        $this->options = $options;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->options['code'];
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->options['label'];
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->options['type'];
    }

    /**
     * @return array
     */
    public function getChoices()
    {
        $options = [];

        if (!empty($this->options['options'])) {
            foreach ($this->options['options'] as $option) {
                $key = $option['key'];
                $value = $option['value'];
                $options[$key] = $value;
            }
        }

        return $options;
    }

    /**
     * @param string $key
     * @return string | null
     */
    public function getChoice($key)
    {
        $choices = $this->getChoices();

        return isset($choices[$key]) ? $choices[$key] : null;
    }
}
