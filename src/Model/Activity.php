<?php
namespace TrekkSoft\SDK\Model;
use TrekkSoft\SDK\Collection\GuestFieldsCollection;

/**
 * Class Activity
 * @package TrekkSoft\SDK\Model
 */
class Activity
{
    /**
     * @var array
     */
    protected $options;

    /**
     * Activity constructor.
     * @param array $options
     */
    public function __construct(array $options)
    {
        $this->setOptions($options);
    }

    /**
     * @param array $options
     */
    protected function setOptions(array $options)
    {
        //set default values
        $options += [
            'id'            => null,
            'title'         => null,
            'description'   => null,
            'guestFields'   => [],
            'departure'     => [],
        ];

        $this->options = $options;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return (int)$this->options['id'];
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->options['title'];
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->options['description'];
    }

    /**
     * @return Location
     */
    public function getDepartureLocation()
    {
        return new Location($this->options['departure']);
    }

    /**
     * @return GuestFieldsCollection
     */
    public function getGuestFields()
    {
        $guestFields = [];

        foreach ($this->options['guestFields'] as $guestField) {
            $guestFields[] = new GuestField($guestField);
        }

        return new GuestFieldsCollection($guestFields);
    }
}
