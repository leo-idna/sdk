<?php
namespace TrekkSoft\SDK\Collection;

use OutOfBoundsException;

abstract class ObjectCollection extends RestrictedArrayCollection
{
    /**
     * @return string
     */
    abstract protected function getElementsClass();

    /**
     * Ensures given element is supported by this collection.
     * This method will throw exception if it is not.
     *
     * @param mixed $element
     * @throws OutOfBoundsException
     * @return void
     */
    protected function ensureSupportedElement($element)
    {
        $requiredClass = $this->getElementsClass();
        $elementType = gettype($element);

        if ($elementType !== 'object') {
            throw new OutOfBoundsException(sprintf(
                'Expected object of class "%s", but element of type "%s" was found', $requiredClass, $elementType
            ));
        } else {
            $elementClass = get_class($element);

            if ($requiredClass !== $elementClass) {
                throw new OutOfBoundsException(sprintf(
                    'Expected object of class "%s", but object of class "%s" was found', $requiredClass, $elementClass
                ));
            }
        }
    }
}
