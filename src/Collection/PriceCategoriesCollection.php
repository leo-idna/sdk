<?php
namespace TrekkSoft\SDK\Collection;

use TrekkSoft\SDK\Model\PriceCategory;

/**
 * Class PriceCategoriesCollection
 * @package TrekkSoft\SDK\Model\Collection
 */
class PriceCategoriesCollection extends ObjectCollection
{
    /**
     * @return string
     */
    protected function getElementsClass()
    {
        return PriceCategory::class;
    }
}
