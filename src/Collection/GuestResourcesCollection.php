<?php
declare(strict_types=1);

namespace TrekkSoft\SDK\Collection;

use TrekkSoft\SDK\Model\GuestResource;

class GuestResourcesCollection extends ObjectCollection
{
    protected function getElementsClass()
    {
        return GuestResource::class;
    }
}
