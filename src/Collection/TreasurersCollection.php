<?php
namespace TrekkSoft\SDK\Collection;

use TrekkSoft\SDK\Model\Treasurer;

/**
 * Class TreasurersCollection
 * @package TrekkSoft\SDK\Model\Collection
 */
class TreasurersCollection extends ObjectCollection
{
    /**
     * @return string
     */
    protected function getElementsClass()
    {
        return Treasurer::class;
    }
}
