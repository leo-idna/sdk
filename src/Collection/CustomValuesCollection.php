<?php
namespace TrekkSoft\SDK\Collection;

use TrekkSoft\SDK\Model\CustomValue;

/**
 * Class CustomValuesCollection
 * @package TrekkSoft\SDK\Model\Collection
 */
class CustomValuesCollection extends ObjectCollection
{
    /**
     * @return string
     */
    protected function getElementsClass()
    {
        return CustomValue::class;
    }
}
