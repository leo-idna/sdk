<?php
namespace TrekkSoft\SDK\Collection;

use TrekkSoft\SDK\Model\Payment;

/**
 * Class PaymentCollection
 * @package TrekkSoft\SDK\Model\Collection
 */
class PaymentsCollection extends ObjectCollection
{
    /**
     * @return string
     */
    protected function getElementsClass()
    {
        return Payment::class;
    }
}
