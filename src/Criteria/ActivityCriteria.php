<?php
namespace TrekkSoft\SDK\Criteria;

/**
 * Class ActivityCriteria
 * @package TrekkSoft\SDK\Criteria
 */
class ActivityCriteria implements MerchantAwareCriteria
{
    use LimitTrait;
    use MerchantTrait;

    /**
     * @var int[]
     */
    private $ids = [];

    /**
     * @var array
     */
    private $departureCityIds = [];

    /**
     * @var string
     */
    private $language = null;

    /**
     * @var string
     */
    private $titleLike = null;

    /**
     * @param int $id
     */
    public function addId($id)
    {
        $this->ids[$id] = (int)$id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->ids = [];
        if ($id) {
            $this->addId($id);
        }
    }

    /**
     * @param int[] $ids
     */
    public function setIds($ids)
    {
        $this->ids = array_combine($ids, $ids);
    }

    /**
     * @return int[]
     */
    public function getIds()
    {
        return array_values($this->ids);
    }

    /**
     * @param int $departureCityId
     */
    public function addDepartureCityId($departureCityId)
    {
        $departureCityId = (int)$departureCityId;
        $this->departureCityIds[$departureCityId] = $departureCityId;
    }

    /**
     * @param int $departureCityId
     */
    public function setDepartureCityId($departureCityId)
    {
        $this->departureCityIds = [];
        if ($departureCityId) {
            $this->addDepartureCityId($departureCityId);
        }
    }

    /**
     * @return int[]
     */
    public function getDepartureCityIds()
    {
        return array_values($this->departureCityIds);
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $titleLike
     */
    public function setTitleLike($titleLike)
    {
        $this->titleLike = $titleLike;
    }

    /**
     * @return string
     */
    public function getTitleLike()
    {
        return $this->titleLike;
    }

    /**
     * @return array
     */
    public function asArray()
    {
        $params = [];

        $params += $this->getLimitParams();
        $params += $this->getMerchantParams();

        if ($ids = $this->getIds()) {
            $params['id'] = $ids;
        }

        if ($departureCityIds = $this->getDepartureCityIds()) {
            $params['departureCityId'] = $departureCityIds;
        }

        if ($language = $this->getLanguage()) {
            $params['language'] = $language;
        }

        if ($titleLike = $this->getTitleLike()) {
            $params['titleLike'] = $titleLike;
        }

        return $params;
    }
}
