<?php
namespace TrekkSoft\SDK\Criteria;

/**
 * Interface MerchantAwareCriteria
 * @package TrekkSoft\SDK\Criteria
 */
interface MerchantAwareCriteria
{
    /**
     * @param string $merchant
     * @return string
     */
    public function addMerchant($merchant);

    /**
     * @param string $merchant
     */
    public function setMerchant($merchant);

    /**
     * @return string[]
     */
    public function getMerchants();

    /**
     * @param string $sellingMerchant
     */
    public function addSellingMerchant($sellingMerchant);

    /**
     * @param string $sellingMerchant
     */
    public function setSellingMerchant($sellingMerchant);

    /**
     * @param string $excludeMerchant
     */
    public function addExcludeMerchant($excludeMerchant);

    /**
     * @param string $excludeMerchant
     */
    public function setExcludeMerchant($excludeMerchant);

    /**
     * @return string[]
     */
    public function getExcludeMerchants();

    /**
     * @return string[]
     */
    public function getSellingMerchants();
}
