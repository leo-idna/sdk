<?php
namespace TrekkSoft\SDK\Criteria;

/**
 * Trait LimitTrait
 * @package TrekkSoft\SDK\Criteria
 */
trait LimitTrait
{
    /**
     * @var int|null
     */
    private $limit = null;

    /**
     * @var int|null
     */
    private $offset = null;

    /**
     * @var int|null
     */
    private $perPage = null;

    /**
     * @return int|null
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     * @return $this
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int|null $offset
     * @return $this
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPerPage()
    {
        return $this->perPage;
    }

    /**
     * @param int|null $perPage
     * @return $this
     */
    public function setPerPage($perPage)
    {
        $this->perPage = $perPage;
        return $this;
    }

    /**
     * @return array
     */
    private function getLimitParams()
    {
        $params = [];

        if ($limit = $this->getLimit()) {
            $params['limit'] = $limit;
        }

        if ($offset = $this->getOffset()) {
            $params['offset'] = $offset;
        }

        return $params;
    }
}
